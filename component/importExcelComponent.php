<?php

function doExcelImport($json)
{

    include('config.inc');
    
    $dumpJsonData = "CALL sp_ImportJsonData('$json')";

    $rsd = mysqli_query($connect_var, $dumpJsonData);
    try {
        $resultArr = array();
        if ($rsd === FALSE) {
            die(mysqli_error($connect_var)); // TODO: better error handling
            echo json_encode(array("status" => "failure", "message" => "Import Failed..!"), JSON_FORCE_OBJECT);
        } else {
            mysqli_close($connect_var);

            $resultArr[0]['status'] = $rsd;
            $resultArr[0]['message'] = "Import Success...";
            echo json_encode( $resultArr[0]);
            //echo json_encode(array("status"=>"success","message"=>"Login Success","result"=>$resultArr[0]),JSON_FORCE_OBJECT);
        }
    } catch (PDOException $e) {
        echo json_encode(array("status" => "error", "message" => $e->getMessage()), JSON_FORCE_OBJECT);
    }
}
