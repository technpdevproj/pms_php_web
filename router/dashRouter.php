<?php

/*************** Get Validate Login ******************/
$f3->route('POST /ValidateLogin', function ($f3) {

	$postdata = file_get_contents("php://input");
	$request = json_decode($postdata,true);

	header('Content-type: application/json');
	header("Access-Control-Allow-Origin: *");
	header('Access-Control-Allow-Headers: X-Requested-With, content-type, access-control-allow-origin, access-control-allow-methods, access-control-allow-headers');

	//$decoded_items =  json_decode($f3->get('BODY'),true); -- NOT Working
	$email = $request->{'Email'};
	$pass =  $request->{'Password'};
	if (!$email == NULL && !$pass == NULL) {
		checkUserLogin($email, $pass);
	} else {
		echo json_encode(array("status" => $request, "message" => "Invalid Parameters..!"), JSON_FORCE_OBJECT);
	}
});
