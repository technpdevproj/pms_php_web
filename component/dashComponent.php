<?php

function checkUserLogin($u, $p)
{

    include('config.inc');

    $getUserList = "CALL sp_ValidateLogin('$u','$p')";

    $rsd = mysqli_query($connect_var, $getUserList);
    try {
        $resultArr = array();
        if ($rsd === FALSE) {
            die(mysqli_error($connect_var)); // TODO: better error handling
        }
        $count = 0;

        while ($rs = mysqli_fetch_assoc($rsd)) {
            $resultArr[$count]['UserID'] = $rs['UserID'];
            $resultArr[$count]['FirstName'] = $rs['FirstName'];
            $resultArr[$count]['LastName'] = $rs['LastName'];
            $resultArr[$count]['Email'] = $rs['Email'];
            $resultArr[$count]['Password'] = $rs['Password'];
            $resultArr[$count]['Mobile'] = $rs['Mobile'];
            $resultArr[$count]['Role_ID'] = $rs['Role_ID'];
            $resultArr[$count]['Role_Code'] = $rs['Role_Code'];
            $resultArr[$count]['Role_Name'] = $rs['Role_Name'];
            $count++;
        }
        mysqli_close($connect_var);

        if ($count > 0) {
            $resultArr[0]['status'] = "success";
            $resultArr[0]['message'] = "Login Success";
            echo json_encode($resultArr[0]);
            //echo json_encode(array("status"=>"success","message"=>"Login Success","result"=>$resultArr[0]),JSON_FORCE_OBJECT);
        } else
            echo json_encode(array("status" => "failure", "message" => "No User Found"), JSON_FORCE_OBJECT);
        // print_r($resultArr);
    } catch (PDOException $e) {
        echo json_encode(array("status" => "error", "message" => $e->getMessage()), JSON_FORCE_OBJECT);
    }
}
