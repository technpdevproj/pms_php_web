<?php

/*************** Get Validate Login ******************/
$f3->route('POST /ImportExcelData', function ($f3) {

	$postdata  = json_decode(file_get_contents('php://input'), true);

	header('Content-type: application/json');
	header("Access-Control-Allow-Origin: *");
	header('Access-Control-Allow-Headers: X-Requested-With, content-type, access-control-allow-origin, access-control-allow-methods, access-control-allow-headers');

	if (!$postdata == NULL) {
		doExcelImport(json_encode($postdata));
	} else {
		echo json_encode(array("status" => "error", "message" => "Invalid Parameters..!"), JSON_FORCE_OBJECT);
	}
});
