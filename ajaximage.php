<?php

header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
session_start();
$session_id='1'; //$session id
$type=$_POST['typeval'];
$path = "uploads/".$type."/";

function getExtension($str) 
{

         $i = strrpos($str,".");
         if (!$i) { return ""; } 

         $l = strlen($str) - $i;
         $ext = substr($str,$i+1,$l);
         return $ext;
 }

	$result = '';
	$status = '';
	$valid_formats = array("jpg", "png", "gif", "bmp","jpeg","PNG","JPG","JPEG","GIF","BMP","pdf","csv","PDF","CSV","xlsx","xls","doc","docx","txt");
	if(isset($_POST) and $_SERVER['REQUEST_METHOD'] == "POST")
		{
			$name = $_FILES['photoimg']['name'];
			$size = $_FILES['photoimg']['size'];
			$ext  = $_FILES['photoimg']['type'];
			$file_ext = strtolower(end(explode('.',$_FILES['photoimg']['name'])));
			$expension = array("jpg", "png", "gif", "jpeg","PNG","JPG","JPEG","GIF","pdf","PDF","txt");
      		$expensions= array("jpeg","jpg","png");
	        if(in_array($file_ext, $expension)===false)
      			{
      				$result = "Invalid file format";
      				$status = 'Failure';
      			}
			else if($size >= 10480000 )
				{
					$result = "Image file size max 1 MB";
					$status = 'Failure';
				}
			else if(strlen($name))
				{
					 $ext = getExtension($name);
					if(in_array($ext,$valid_formats))
						{
							$actual_image_name = time().substr(str_replace(" ", "_", $txt), 5).".".$ext;
							$tmp = $_FILES['photoimg']['tmp_name'];
							if(move_uploaded_file($tmp, $path.$actual_image_name))
								{
									$result = "uploads/".$type."/".$actual_image_name;
									$status = 'Success';
								}
							else{
								$result = "Fail upload folder with read access.";
								$status = 'Failure';
							}
						}
				}
				
			else {
				$result = "Please select image..!";
				$status = 'Failure';
			}
			echo json_encode(array("status"=>$status, "response"=>$result),JSON_FORCE_OBJECT);
			exit;
		}
	

?>