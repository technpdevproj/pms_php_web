<?php

// Kickstart the framework
$f3=require('lib/base.php');

$f3->config('config.ini');

header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");


/****** PMS Modules  *****/
require 'component/dashComponent.php';
require 'router/dashRouter.php';

require 'component/importExcelComponent.php';
require 'router/importExcelRouter.php';

$f3->route('GET /',
	function($f3) {
		
		echo "<b>Welcome TO PMS</b>";
	}
);
$f3->run();